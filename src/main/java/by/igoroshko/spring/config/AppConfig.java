package by.igoroshko.spring.config;

import by.igoroshko.spring.dao.CompanyDao;
import by.igoroshko.spring.dao.EmployeeDao;
import by.igoroshko.spring.dao.LogsDao;
import by.igoroshko.spring.dao.ProjectDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    static {
        System.out.println("FROM APPCONFIG CLASS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    @Bean
    public CompanyDao getCompanyDao() {
        return new CompanyDao();
    }

    @Bean
    public EmployeeDao getEmployeeDao() {
        return new EmployeeDao();
    }

    @Bean
    public LogsDao getLogsDao() {
        return new LogsDao();
    }

    @Bean
    public ProjectDao getProjectDao() {
        return new ProjectDao();
    }


}
