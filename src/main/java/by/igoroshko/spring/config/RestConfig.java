package by.igoroshko.spring.config;

import by.igoroshko.spring.service.CompanyService;
import by.igoroshko.spring.service.EmployeeService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/*")
public class RestConfig extends Application {

    static {
        System.out.println("RESTCONFIG!!!!!!!!!!");
    }

    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(Arrays.asList(
                CompanyService.class,
                EmployeeService.class
        ));
    }
}
