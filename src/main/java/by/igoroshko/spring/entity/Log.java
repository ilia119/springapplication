package by.igoroshko.spring.entity;

public class Log {
    private Employee employee;
    private Project project;
    private int secondTime;
    private String comment;

    public Log() {
    }

    public Log(Employee employee, Project project, int secondTime, String comment) {
        this.employee = employee;
        this.project = project;
        this.secondTime = secondTime;
        this.comment = comment;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getSecondTime() {
        return secondTime;
    }

    public void setSecondTime(int secondTime) {
        this.secondTime = secondTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
