package by.igoroshko.spring.entity;

import java.util.LinkedList;
import java.util.List;

public class Company {
    private int id;
    private String logoUrl;
    private String name;

    private List<Project> projects;
    private List<Employee> employees;

    public Company() {
    }

    public Company(int id, String logoUrl, String name) {
        this.id = id;
        this.logoUrl = logoUrl;
        this.name = name;
        projects = new LinkedList<>();
        employees = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

}
