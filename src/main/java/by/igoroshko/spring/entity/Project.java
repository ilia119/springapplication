package by.igoroshko.spring.entity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Project {
    private int id;
    private String logoUrl;
    private int manHours;
    private Date startDate;
    private Date endDate;

    private List<Employee> employees;

    public Project() {
    }

    public Project(int id, String logoUrl, int manHours, Date startDate, Date endDate) {
        this.id = id;
        this.logoUrl = logoUrl;
        this.manHours = manHours;
        this.startDate = startDate;
        this.endDate = endDate;
        employees = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public int getManHours() {
        return manHours;
    }

    public void setManHours(int manHours) {
        this.manHours = manHours;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

}
