package by.igoroshko.spring.entity;

import java.util.LinkedList;
import java.util.List;

public class Employee {
    private int id;
    private String photoUrl;
    private String name;
    private String email;

    private List<Project> projects = new LinkedList<>();

    public Employee() {
    }

    public Employee(int id, String photoUrl, String name, String email) {
        this.id = id;
        this.photoUrl = photoUrl;
        this.name = name;
        this.email = email;
    }

    public void assignToProject(Project project) {
        //TODO
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

}
