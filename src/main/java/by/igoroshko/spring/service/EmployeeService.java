package by.igoroshko.spring.service;

import by.igoroshko.spring.dao.EmployeeDao;
import by.igoroshko.spring.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/employee")
public class EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getAll() {
        return employeeDao.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Employee getById(@PathParam("id") int id) {
        return employeeDao.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEmployee(Employee employee, @Context UriInfo uriInfo) {
        employeeDao.save(new Employee(
                employee.getId(),
                employee.getPhotoUrl(),
                employee.getName(),
                employee.getEmail()));
        return Response.status(Response.Status.CREATED.getStatusCode()).header("Location", String.format("%s/%s", uriInfo.getAbsolutePath().toString(), employee.getId())).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEmployee(@PathParam("id") int id) {
        employeeDao.delete(id);
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }


}
