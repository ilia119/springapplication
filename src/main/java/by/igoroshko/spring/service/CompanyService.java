package by.igoroshko.spring.service;

import by.igoroshko.spring.dao.CompanyDao;
import by.igoroshko.spring.entity.Company;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;


@Path("/company")
public class CompanyService {
    public static final Logger log = Logger.getLogger(CompanyService.class);

    @Autowired
    private CompanyDao companyDao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Company> getAll() {
        log.fatal("Start getAll method");
        return companyDao.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Company getById(@PathParam("id") int id) {
        return companyDao.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCompany(Company company, @Context UriInfo uriInfo) {
        companyDao.save(new Company(
                company.getId(),
                company.getLogoUrl(),
                company.getName()));
        return Response.status(Response.Status.CREATED.getStatusCode()).header("Location", String.format("%s/%s", uriInfo.getAbsolutePath().toString(), company.getId())).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteCompany(@PathParam("id") int id) {
        companyDao.delete(id);
        return Response.status(Response.Status.OK.getStatusCode()).build();
    }

}
