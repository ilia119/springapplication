package by.igoroshko.spring.service;

import by.igoroshko.spring.dao.ProjectDao;
import by.igoroshko.spring.entity.Project;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/project")
public class ProjectService {

    @Autowired
    private ProjectDao projectDao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Project> getAll() {
        return projectDao.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Project getById(@PathParam("id") int id) {
        return projectDao.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addProject(Project project, @Context UriInfo uriInfo) {
        return null;
    }




    public void addProject() {
        //TODO
    }
}
