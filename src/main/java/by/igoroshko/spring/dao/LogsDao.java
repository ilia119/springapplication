package by.igoroshko.spring.dao;

import by.igoroshko.spring.entity.Log;

import java.util.List;

public class LogsDao {

    private List<Log> logs;

    public Log findById(int id) {
        return logs.get(id);
    }

    public void save(Log log) {
        logs.add(log);
    }

    public void delete(int id) {
        logs.remove(id);
    }

}
