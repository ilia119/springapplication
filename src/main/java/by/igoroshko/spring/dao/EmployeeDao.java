package by.igoroshko.spring.dao;

import by.igoroshko.spring.entity.Employee;

import java.util.LinkedList;
import java.util.List;

public class EmployeeDao {

    static {
        System.out.println("EMPLOYEEE DAO!!!!!!!!!!!");
    }

    private List<Employee> employees;

    public EmployeeDao() {
        employees = new LinkedList<>();
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(int id) {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        throw new IndexOutOfBoundsException("No any employee with " + id + " id");
    }

    public void save(Employee employee) {
        employees.add(employee);
    }

    public void delete(int id) {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                employees.remove(employee);
                return;
            }
        }
        throw new IndexOutOfBoundsException("No any employee with " + id + " id");
    }
}
