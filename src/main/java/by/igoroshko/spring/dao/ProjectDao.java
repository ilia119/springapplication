package by.igoroshko.spring.dao;

import by.igoroshko.spring.entity.Project;

import java.util.List;

public class ProjectDao {

    private List<Project> projects;

    public List<Project> findAll() {
        return projects;
    }

    public Project findById(int id) {
        for (Project project : projects) {
            if (project.getId() == id) {
                return project;
            }
        }
        throw new IndexOutOfBoundsException("No any project with " + id + " id");
    }

    public void save(Project project) {
        projects.add(project);
    }

    public void delete(int id) {
        for (Project project : projects) {
            if (project.getId() == id) {
                projects.remove(project);
                return;
            }
        }
        throw new IndexOutOfBoundsException("No any project with " + id + " id");

    }
}
