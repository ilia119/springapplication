package by.igoroshko.spring.dao;

import by.igoroshko.spring.entity.Company;

import java.util.LinkedList;
import java.util.List;

public class CompanyDao {

    private List<Company> companies;

    static {
        System.out.println("Company DAO!!!!!!!!!!!");
    }

    public CompanyDao() {
        companies = new LinkedList<>();
        companies.add(new Company(1, "URL", "NAME"));
        companies.add(new Company(2, "ljsdkf", "Kjhsdf"));
    }


    public List<Company> findAll() {
        return companies;
    }


    public Company findById(int id) {
        for (Company company : companies) {
            if (company.getId() == id) {
                return company;
            }
        }
        throw new IndexOutOfBoundsException("No any company with " + id + " id!!!");
    }


    public void save(Company company) {
        companies.add(company);
    }


    public void delete(int id) {
        for (Company company : companies) {
            if (company.getId() == id) {
                companies.remove(company);
                return;
            }
        }
        throw new IndexOutOfBoundsException("No any company with " + id + " id!!!");
    }
}
